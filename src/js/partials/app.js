(function($){
    "use strict";

    //$('.main-slider').slick({
    //    infinite: true,
    //    slidesToShow: 1,
    //    slidesToScroll: 1,
    //    arrows: true,
    //    dots: false,
    //    autoplay: true,
    //    autoplaySpeed: 4000
    //});

    $('.useful-links-slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 4000
    });

})($);